# Description and installation instructions

## Description

A web app that allows you to edit/delete registered users and manage different versions of terms of service. There are also some cli command that can help with managing the users and terms.

### Authentication

Most of the web app requires the user to be authenticated (logged in) before it can be used.  
If the user attempts to access a page that requires authentication but is not logged in, they will be redirect to the login page.  

A user with login credential can log in. If the user attempts too many logins (10) with invalid credentials in a short amount of time (30 seconds), they will be blocked from further attempts until the timer expires and this event will be logged.  

A user that doesn't have login credentials can register a new account by using the "register" link in the top right of the page. The app requires the password to be at least 8 characters long, contain 1 lowercase, 1 uppercase, 1 number and 1 symbol to make the password more secure. The terms of service must also be accepted. After clicking the register button, the user now has to validate their email and is prompted to do so. The web app automatically sends an email with validation instructions. Once the user validates their email, they can access the entire web app.

### Managing users

By visiting `/dashboard` the user can edit, delete and unverify users. These actions can be performed even for the authenticated user (they can delete their own user).  
There is a search input on the page which can be used to filter the list. The search matches exactly the entire or parts of the name and/or email and/or phone number.  

**Deleting** a user removes the user from the web app entirely. This can break the web app for the user if they are using it.  

**Unverifying** a user prompts the user to verify their email address again. An email will be sent to them with verification instructions. They can't access the site fully until they verify their email.

**Editing** a user allows the authenticated user to change info about the user that is edited.  While changing the name and phone number has no further effect than changing the data, changing the email will prompt the user to verify their **new** email. An email will be sent to their new email address with confirmation instructions.  
Changing the password is done by filling the password fields.  
Be aware that changing the email and/or the password will leave that user unable to login with their old credentials.

### Managing terms of service

By visiting `/terms_of_service`, the user can add new terms, edit/delete/publish unpublished terms and view published terms.  

Publishing a new "terms of service" will send a notification email to all users notifying them of the change and prompting them to accept the new terms.  

Also, if a user has accepted outdated terms, they will be prompted on every page to accept the new terms.

### CLI commands

`php artisan user:unverify {userId}` can be used to unverify a user id. The effect is the same as using the `unverify` button on `/dashboard`. An email will be sent to the user with the id `userId`, if the user exists.

`php artisan tos:delete-orphans` can be used to delete all published terms that are not the "Currently Accepted Terms" of any users.

## Installation instructions

The web app is essentially a Laravel web app.  
Minimum server requirements can be found in the official documentation [https://laravel.com/docs/8.x#server-requirements]()  

Installation and deployment instructions can be found on the official site: [Installation](https://laravel.com/docs/8.x/installation)  
[Deployment](https://laravel.com/docs/8.x/deployment#introduction)

Instead of creating a new project you have to extract the contents of this archive.



 




