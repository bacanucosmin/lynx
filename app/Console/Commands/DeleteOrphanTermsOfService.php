<?php

namespace App\Console\Commands;

use App\Models\TermsOfService;
use Illuminate\Console\Command;

class DeleteOrphanTermsOfService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tos:delete-orphans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all published terms that are not the
"Currently Accepted Terms" of any users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->confirm('This will also delete the most recent published ToS if no one has accepted it yet. Continue?');

        TermsOfService::whereNotNull('publication_date')
            ->leftJoin('users', 'users.terms_of_service_id', '=', 'terms_of_services.id')
            ->whereNull('users.terms_of_service_id')
            ->delete();

        $this->info('Deleted orphan published ToS.');

        return 0;
    }
}
