<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class UnverifyUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:unverify
                            {userId : A valid user id} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Unverify a user by their id. An email will be sent to the user with instructions on how to verify their email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userId = $this->argument('userId');
        $user = User::find($userId);

        if (! $user) {
            $this->error("No user found with id {$userId}");

            return 1;
        }

        $user->unverify();

        $this->info("User with id {$userId} has been unverified");

        return 0;
    }
}
