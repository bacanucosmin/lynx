<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\TermsOfService;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Rules\SecurePassword;
use App\Rules\StorableString;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => [
                'required',
                'string',
                new StorableString(),
            ],
            'phone_number' => [
                'string',
                new StorableString(),
            ],
            'email' => [
                'required',
                'string',
                'email',
                'unique:users',
                new StorableString(),
            ],
            'terms_of_service' => ['accepted'],
            'password' => [
                'required',
                'string',
                'confirmed',
                new SecurePassword,
            ],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User;

        $user->forceFill([
            'name' => $data['name'],
            'email' => $data['email'],
            'terms_of_service_accepted_at' => now(),
            'terms_of_service_id' => TermsOfService::getMostRecentPublished()->id,
            'phone_number' => $data['phone_number'],
            'password' => Hash::make($data['password']),
        ])->save();

        return $user;
    }
}
