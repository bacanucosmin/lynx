<?php

namespace App\Http\Controllers;

use App\Http\Requests\AcceptNewTermsOfServiceRequest;
use App\Http\Requests\TermsOfServiceStoreRequest;
use App\Http\Requests\TermsOfServiceUpdateRequest;
use App\Models\TermsOfService;
use Illuminate\Contracts\Auth\Authenticatable;

class TermsOfServiceController extends Controller
{
    public function index()
    {
        $items = TermsOfService::all();

        return view('terms_of_service.index', [
            'items' => $items,
        ]);
    }

    public function create()
    {
        return view('terms_of_service.create');
    }

    public function store(TermsOfServiceStoreRequest $request)
    {
        TermsOfService::create($request->only(['administrative_name', 'content']));

        return redirect()
            ->route('terms_of_service.index')
            ->with('success', ['Terms of service created!']);
    }

    public function show(TermsOfService $termsOfService)
    {
        return view('terms_of_service.show', [
            'item' => $termsOfService,
        ]);
    }

    public function edit(TermsOfService $termsOfService)
    {
        if ($termsOfService->isPublished()) {
            abort(401, "Can't edit publised terms.");
        }

        return view('terms_of_service.edit', [
            'item' => $termsOfService,
        ]);
    }

    public function update(TermsOfServiceUpdateRequest $request, TermsOfService $termsOfService)
    {
        if ($termsOfService->isPublished()) {
            abort(401, "Can't edit publised terms.");
        }

        $termsOfService->update($request->only(['administrative_name', 'content']));

        if ($request->publish) {
            $termsOfService->publish();

            return redirect()->route('terms_of_service.index')
                ->with('success', ['Terms of service updated and published!']);
        }

        return redirect()->route('terms_of_service.index')
            ->with('success', ['Terms of service updated!']);
    }

    public function destroy(TermsOfService $termsOfService)
    {
        if ($termsOfService->isPublished()) {
            abort(401, "Can't delete published terms.");
        }

        $termsOfService->delete();

        return back()->with(['success' => ['Terms of service deleted']]);
    }

    /**
     * Show the current terms of service.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function current()
    {
        $termsOfService = TermsOfService::getMostRecentPublished();

        return view('terms_of_service.show', [
            'item' => $termsOfService,
        ]);
    }

    /**
     * Make sure that the accepted tos is the current tos before updating the user.
     *
     * @param AcceptNewTermsOfServiceRequest $request
     * @param Authenticatable $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptNewTermsOfService(AcceptNewTermsOfServiceRequest $request, Authenticatable $user)
    {
        if ((int) $request->terms_of_service_id !== TermsOfService::getMostRecentPublished()->id) {
            return redirect()->route('terms_of_service.current')->withErrors(['terms_of_service_id' => 'You are not accepting the latest terms of service. Try again.']);
        }

        $user->acceptTermsOfService(TermsOfService::getMostRecentPublished());

        return back()->with(['success' => ['You\'ve accepted the new terms of service']]);
    }
}
