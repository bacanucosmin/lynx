<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * @param Request $request
     * @return User[]|Collection
     */
    public function index(Request $request)
    {
        if ($search = trim($request->search)) {
            return User::search($search)->get();
        }

        return User::all();
    }

    /**
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        return view('users.edit', [
            'user' => $user,
        ]);
    }

    /**
     * @param UpdateUserRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $messages = [];

        $user->update($request->only(['name', 'phone_number']));
        $messages[] = 'Changes have been saved.';

        if ($user->email !== $request->email) {
            $user->changeEmail($request->email);
            $messages[] = 'A verification email has been sent to the new address.';
        }

        if ($request->password && ! Hash::check($request->password, $user->password)) {
            $user->changePassword($request->password);
            $messages[] = 'The password has been changed.';
        }

        return back()->with('success', $messages);
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
    }

    /**
     * @param User $user
     * @return User
     */
    public function unverify(User $user)
    {
        $user->unverify();

        return $user;
    }
}
