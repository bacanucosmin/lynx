<?php

namespace App\Http\Requests;

use App\Rules\StorableString;
use Illuminate\Foundation\Http\FormRequest;

class TermsOfServiceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'administrative_name' => [
                'required',
                'string',
                new StorableString(),
            ],
            'publish' => ['sometimes'],
            'content' => ['required_with:publish'],
        ];
    }

    public function messages()
    {
        return [
            'required_with' => 'The content field is required in order to publish these terms.',
        ];
    }
}
