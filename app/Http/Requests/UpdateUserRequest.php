<?php

namespace App\Http\Requests;

use App\Rules\SecurePassword;
use App\Rules\StorableString;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
                new StorableString(),
            ],
            'phone_number' => [
                'string',
                new StorableString(),
            ],
            'email' => [
                'required',
                'string',
                'email',
                new StorableString(),
                Rule::unique('users')->ignore($this->route()->parameter('user')),
            ],
            'password' => [
                'nullable',
                'required_with:current_password',
                'confirmed',
                new SecurePassword,
            ],
        ];
    }
}
