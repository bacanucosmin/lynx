<?php

namespace App\Jobs;

use App\Models\TermsOfService;
use App\Models\User;
use App\Notifications\TermsOfServiceHaveChanged;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyUsersAboutTermsOfServiceChanges implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentTermsOfService = TermsOfService::getMostRecentPublished();

        $notifiables = User::where('terms_of_service_id', '!=', $currentTermsOfService->id)
            ->whereNotNull('email_verified_at')
            ->get();

        foreach ($notifiables as $notifiable) {
            $notifiable->notify(new TermsOfServiceHaveChanged());
        }
    }
}
