<?php

namespace App\Models;

use App\Jobs\NotifyUsersAboutTermsOfServiceChanges;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermsOfService extends Model
{
    use HasFactory;

    protected $fillable = [
        'administrative_name',
        'content',
    ];

    protected $dates = [
        'publication_date',
    ];

    public function isPublished()
    {
        return ! is_null($this->publication_date);
    }

    /**
     * Publishes the terms of service
     * This sends a notification to users that haven't accepted the current tos (all of them).
     */
    public function publish()
    {
        $this->forceFill(['publication_date' => now()])->save();
        dispatch(new NotifyUsersAboutTermsOfServiceChanges());
    }

    public static function getMostRecentPublished()
    {
        return self::whereNotNull('publication_date')
            ->orderBy('publication_date', 'desc')
            ->orderBy('id', 'desc')
            ->firstOrFail();
    }
}
