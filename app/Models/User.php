<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'action_urls',
        'has_verified_email',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'terms_of_service_accepted_at' => 'datetime',
    ];

    /**
     * @return bool
     */
    public function getHasVerifiedEmailAttribute()
    {
        return $this->hasVerifiedEmail();
    }

    /**
     * A list of urls for actions that can be performed on the user.
     *
     * @return array
     */
    public function getActionUrlsAttribute()
    {
        return [
            'edit' => route('users.edit', $this->id),
            'destroy' => route('users.destroy', $this->id),
            'unverify' => route('users.unverify', $this->id),
        ];
    }

    /**
     * A custom scope to be used for filtering on the users dashboard.
     *
     * @param $query
     * @param string $term
     */
    public function scopeSearch($query, string $term)
    {
        $query->where(function ($q) use ($term) {
            $q->orWhere('email', 'LIKE', '%'.$term.'%')
                ->orWhere('name', 'LIKE', '%'.$term.'%')
                ->orWhere('phone_number', 'LIKE', '%'.$term.'%');
        });
    }

    /**
     * Unverifies the email of the user.
     */
    public function unverify()
    {
        $this->forceFill(['email_verified_at' => null])
            ->save();
    }

    /**
     * The terms of service accepted by the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function acceptedTermsOfService()
    {
        return $this->belongsTo(TermsOfService::class, 'terms_of_service_id');
    }

    public function hasAcceptedLatestTermsOfService()
    {
        return $this->acceptedTermsOfService->id === TermsOfService::getMostRecentPublished()->id;
    }

    public function acceptTermsOfService(TermsOfService $termsOfService)
    {
        $this->forceFill([
            'terms_of_service_id' => $termsOfService->id,
            'terms_of_service_accepted_at' => now(),
        ])->save();
    }

    public function changeEmail(string $email)
    {
        $this->forceFill([
            'email_verified_at' => null,
            'email' => $email,
        ])->save();

        $this->sendEmailVerificationNotification();
    }

    public function changePassword(string $password)
    {
        $this->forceFill([
            'password' => Hash::make($password),
        ])->save();
    }
}
