<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StorableString implements Rule
{
    protected $maxLength = 255;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return strlen($value) <= $this->maxLength;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The :attribute attribute must have a max length of {$this->maxLength}";
    }
}
