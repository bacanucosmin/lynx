<?php

namespace Database\Factories;

use App\Models\TermsOfService;
use Illuminate\Database\Eloquent\Factories\Factory;

class TermsOfServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TermsOfService::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'publication_date' => rand(0, 1) ? now() : null,
            'administrative_name' => $this->faker->sentence,
            'content' => $this->faker->text(),
        ];
    }
}
