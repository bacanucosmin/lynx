<?php

namespace Database\Factories;

use App\Models\TermsOfService;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'phone_number' => $this->faker->phoneNumber,
            'email_verified_at' => rand(0, 1) ? now() : null,
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'terms_of_service_id' => TermsOfService::getMostRecentPublished()->id,
            'terms_of_service_accepted_at' => now(),
        ];
    }

    public function ideal()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => now(),
            ];
        });
    }
}
