<?php

namespace Database\Seeders;

use App\Models\TermsOfService;
use Illuminate\Database\Seeder;

class TermsOfServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TermsOfService::factory()->create([
            'publication_date' => null,
        ]);
        TermsOfService::factory()->create([
            'publication_date' => now()->subDay(),
        ]);
        TermsOfService::factory()->create([
            'publication_date' => now(),
        ]);

        TermsOfService::factory()->count(5)->create([
            'publication_date' => now(),
        ]);
    }
}
