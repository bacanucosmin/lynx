<?php

namespace Database\Seeders;

use App\Models\TermsOfService;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $expiredPublishedTerms = TermsOfService::whereNotNull('publication_date')
            ->orderBy('publication_date', 'asc')
            ->firstOrFail();

        $user = User::factory()->create([
            'email' => 'test@example.com',
            'email_verified_at' => now(),
        ]);

        $user->acceptTermsOfService($expiredPublishedTerms);

        User::factory()->count(10)->create();
    }
}
