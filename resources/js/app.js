require('./bootstrap');
import Vue from 'vue';

window.initUsersList = function (usersIndexUrl) {
    new Vue({
        el: '#users-list',
        data: {
            users: [],
            searchTerm: "",
        },
        methods: {
            deleteUser(user) {
                axios.delete(user.action_urls.destroy)
                    .then((res) => {
                        this.users = this.users.filter((u) => u.id !== user.id)
                    });
            },
            unverify(user) {
                axios.put(user.action_urls.unverify)
                    .then((res) => {
                        this.users = this.users.map((u) => {
                            if (u.id === user.id) {
                                return res.data;
                            }

                            return u;
                        })
                    });
            },
            search() {
                axios.get(usersIndexUrl, {params: {search: this.searchTerm}})
                    .then((res) => {
                        this.users = res.data;
                    });
            }
        },
        created() {
            axios.get(usersIndexUrl)
                .then((res) => {
                    this.users = res.data;
                });
        }
    });
}
