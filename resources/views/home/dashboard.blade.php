@extends('layouts.app')

@section('content')
    <div class="container" id="users-list">
        <form class="mb-4" @submit.prevent="search(searchTerm)">

            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" v-model="searchTerm" placeholder="">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>

        <div class="row justify-content-center">
            <div class="col">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody is="transition-group" name="list">
                    <tr v-for="user in users" :key="user.id" class="list-item">
                        <td>@{{ user.id }}</td>
                        <td>@{{ user.email }}</td>
                        <td>@{{ user.phone_number }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" :href="user.action_urls.edit">Edit</a>
                            <button class="btn btn-sm btn-danger" @click="deleteUser(user)">Delete</button>
                            <button class="btn btn-sm btn-warning"
                                    v-if="user.has_verified_email"
                                    @click="unverify(user)">Unverify
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            initUsersList('{{ route('users.index') }}')
        })
    </script>
@endsection
