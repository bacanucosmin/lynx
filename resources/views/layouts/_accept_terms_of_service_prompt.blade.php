<div class="alert alert-danger">
    Please accept the new terms of service. <br>
    <a href="{{ route('terms_of_service.show', auth()->user()->acceptedTermsOfService) }}">Terms you've accepted on {{ auth()->user()->terms_of_service_accepted_at->toIsoString() }} (old)</a> <br>
    <a href="{{ route('terms_of_service.current') }}">New terms that you must read and accept, published on {{ \App\Models\TermsOfService::getMostRecentPublished()->publication_date->toIsoString() }} (new)</a> <br>
    <form method="POST" action="{{ route('terms_of_service.accept_new') }}" >
        @csrf
        <input type="hidden" name="terms_of_service_id" value="{{ \App\Models\TermsOfService::getMostRecentPublished()->id }}">
        <button type="submit" class="btn btn-primary">Click here to accept the new terms of service</button>
    </form>

</div>
