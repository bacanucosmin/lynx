@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="mb-4">
            <div class="row">
                <div class="col">
                    <a href="{{ route('terms_of_service.create') }}" class="btn btn-primary">Add new terms</a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Publication date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->administrative_name }}</td>
                            <td>{{ $item->publication_date ? $item->publication_date->format('Y-m-d H:i') : 'not published' }}</td>
                            <td>
                                @if($item->publication_date)
                                <a class="btn btn-sm btn-secondary" href="{{ route('terms_of_service.show', $item) }}">View</a>
                                @else
                                <a class="btn btn-sm btn-primary" href="{{ route('terms_of_service.edit', $item) }}">Edit</a>
                                <form method="POST" action="{{ route('terms_of_service.destroy', $item) }}" style="display: inline">
                                    @csrf
                                    @method("DELETE")
                                    <button class="btn btn-sm btn-danger" >Delete</button>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
