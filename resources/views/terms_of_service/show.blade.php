@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">
            <div class="col">
                <h4>Published: {{ $item->publication_date->toISOString() }}</h4>
                <p>{!! $item->content !!}</p>
            </div>
        </div>
    </div>
@endsection
