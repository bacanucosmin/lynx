<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TermsOfServiceController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/current_terms_of_service', [TermsOfServiceController::class, 'current'])->name('terms_of_service.current');

Route::group([
    'middleware' => ['auth', 'verified'],
], function () {
    Route::get('/', function () {
        return redirect(\App\Providers\RouteServiceProvider::HOME);
    });

    Route::get('dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
    Route::put('users/{user}/unverify', [UsersController::class, 'unverify'])->name('users.unverify');
    Route::resource('users', UsersController::class)
        ->only(['index', 'update', 'destroy', 'edit']);

    Route::post('terms_of_service/accept_new', [TermsOfServiceController::class, 'acceptNewTermsOfService'])->name('terms_of_service.accept_new');

    Route::resource('terms_of_service', TermsOfServiceController::class);
});
