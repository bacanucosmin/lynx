<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        for ($i = 0; $i < 10; $i++) {
            $response = $this->post(route('login'), ['email' => 'test@example.com', 'password' => 'test']);
            $response->assertSessionHasErrors(['email' => 'These credentials do not match our records.']);
        }

        $response = $this->post(route('login'), ['email' => 'test@example.com', 'password' => 'test']);

        $errors = session('errors');

        //workaround for flaky test
        $this->assertTrue(
            str_contains($errors->get('email')[0], 'Too many login attempts. Please try again in ')
        );
    }
}
