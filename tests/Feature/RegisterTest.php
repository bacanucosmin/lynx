<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use DatabaseTransactions;

    public function testCanBeAccessedAsGuest()
    {
        $response = $this->get(route('register'));
        $response->assertStatus(200);
    }

    public function testCantBeAccessedWhenAuthenticated()
    {
        $user = User::factory()->ideal()->create();

        $this->actingAs($user);

        $response = $this->get(route('register'));
        $response->assertRedirect(route('dashboard'));
    }

    public function testAcceptingTheTermsIsMandatoryOnRegistration()
    {
        $res = $this->post(route('register'), [
            '_token' => csrf_token(),
            'name' => 'test',
            'phone_number' => 'test',
            'email' => 'test@test.test',
            'password' => 'Ab#123456',
            'password_confirmation' => 'Ab#123456',
        ]);

        $res->assertSessionHasErrors('terms_of_service');

        $res = $this->post(route('register'), [
            '_token' => csrf_token(),
            'name' => 'test',
            'phone_number' => 'test',
            'email' => 'test@test.test',
            'password' => 'Ab#123456',
            'password_confirmation' => 'Ab#123456',
            'terms_of_service' => true,
        ]);

        $res->assertRedirect(route('dashboard'));
    }

    public function testUsersAreRedirectedToDashboardAfterVerifyingEmail()
    {
        $user = User::factory()->create([
            'email_verified_at' => null,
        ]);

        $this->actingAs($user);

        $res = $this->get(route('verification.notice'));
        $res->assertStatus(200);

        $user->forceFill(['email_verified_at' => now()])->save();

        //The default verification controller redirects to the same path if accessing as verified
        //or if just completing verification
        $res = $this->get(route('verification.notice'));
        $res->assertRedirect(route('dashboard'));
    }
}
