<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class UnverifyUserCommandTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testItExitsWithErrorIfUserDoesntExist()
    {
        $this->artisan('user:unverify', ['userId' => 99999])
            ->assertExitCode(1);
    }

    public function testItUnverifiesExistingUsers()
    {
        $user = User::factory()->ideal()->create();
        $this->artisan('user:unverify', ['userId' => $user->id])
            ->assertExitCode(0);

        $this->assertFalse($user->fresh()->hasVerifiedEmail());
    }
}
