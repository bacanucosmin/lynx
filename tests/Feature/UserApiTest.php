<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserApiTest extends TestCase
{
    public function testApiShouldOnlyBeAccessibleToAuthenticatedUsers()
    {
        $res = $this->getJson(route('users.index'));
        $res->assertStatus(401);

        $user = User::factory()->ideal()->create();

        $this->actingAs($user);

        $res = $this->getJson(route('users.index'));
        $res->assertStatus(200);
    }

    public function testApiFilters()
    {
        $key = Str::random();

        User::factory()->count(10)->create();
        User::factory()->create(['name' => $key]);
        User::factory()->create(['email' => $key]);
        User::factory()->create(['phone_number' => $key]);

        $user = User::factory()->ideal()->create();
        $this->actingAs($user);

        $res = $this->getJson(route('users.index', ['search' => strtolower($key)]));
        $res->assertJsonCount(3);
    }

    public function testDeleteUser()
    {
        $user = User::factory()->ideal()->create();
        $this->assertDatabaseHas('users', ['id' => $user->id]);

        $this->actingAs($user);

        $this->deleteJson(route('users.destroy', $user));
        $this->assertDatabaseMissing('users', ['id' => $user->id]);
    }

    public function testUnverifyUser()
    {
        /** @var User $user */
        $user = User::factory()->ideal()->create();
        $this->actingAs($user);

        $this->putJson(route('users.unverify', $user));

        $this->assertFalse($user->fresh()->hasVerifiedEmail());
    }
}
