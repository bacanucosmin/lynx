<?php

namespace Tests\Unit;

use App\Rules\SecurePassword;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class RulesTest extends TestCase
{
    public function testSecurePasswordEnforcesRequirements()
    {
        $data = [
            'a' => true,
            '12345678' => true,
            'a2345678' => true,
            'aB345678' => true,
            'aB#45678' => false,
        ];

        foreach ($data as $pass => $expected) {
            $actual = Validator::make(
                ['pass' => $pass],
                ['pass' => [new SecurePassword()]]
            )->errors()->has('pass');

            $this->assertEquals($expected, $actual, "Expected validation fail for {$pass}");
        }
    }
}
